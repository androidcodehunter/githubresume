package com.sharif.githubresume.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sharif.githubresume.api.GithubService
import com.sharif.githubresume.api.GithubService.Companion.BASE_URL
import com.sharif.githubresume.api.GithubService.Companion.DATE_FORMAT
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val netWorkModule = module {
    factory { provideGson() }
    factory { provideGithubApi(get()) }
    factory { provideOkHttpClient() }
    single { provideRetrofit(get(), get()) }
}

fun provideGson(): Gson{
    return GsonBuilder().setDateFormat(DATE_FORMAT).create()
}

fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
    return Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson)).build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder().build()
}

fun provideGithubApi(retrofit: Retrofit): GithubService = retrofit.create(GithubService::class.java)