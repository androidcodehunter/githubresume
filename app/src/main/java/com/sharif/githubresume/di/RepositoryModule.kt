package com.sharif.githubresume.di

import com.sharif.githubresume.db.AppDatabase
import com.sharif.githubresume.data.local.ResumeLocalDataSource
import com.sharif.githubresume.data.remote.ResumeRemoteDataSource
import com.sharif.githubresume.data.source.ResumeRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val resumeRepositoryModule = module {
    factory { ResumeLocalDataSource(get()) }
    factory { ResumeRemoteDataSource(get()) }
    factory { AppDatabase(androidContext()) }
    factory { ResumeRepositoryImpl(get() as ResumeLocalDataSource, get() as ResumeRemoteDataSource) }
}