package com.sharif.githubresume.di

import com.sharif.githubresume.data.source.ResumeRepositoryImpl
import com.sharif.githubresume.ui.ResumeViewModel
import org.koin.dsl.module

val resumeViewModel = module {
    factory { ResumeViewModel(get() as ResumeRepositoryImpl) }
}