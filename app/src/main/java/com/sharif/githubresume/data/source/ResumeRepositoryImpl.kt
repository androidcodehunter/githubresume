package com.sharif.githubresume.data.source

import com.sharif.githubresume.vo.Repo
import com.sharif.githubresume.vo.Result
import com.sharif.githubresume.vo.User
import timber.log.Timber
import java.lang.Exception

class ResumeRepositoryImpl(private val resumeLocalDataSource: ResumeDataSource,
                           private val resumeRemoteDataSource: ResumeDataSource) : ResumeRepository {

    override suspend fun getUser(userName: String): Result<User?> {
        try {
            updateUserFromRemoteDataSource(userName)
        }catch (ex: Exception){
            Timber.d("Insertion exception $ex")
            //return Result.Error(ex)
        }
        return resumeLocalDataSource.getUser(userName)
    }

    private suspend fun updateUserFromRemoteDataSource(userName: String) {

             val response = resumeRemoteDataSource.getUser(userName)
            if (response is Result.Success){
                response.data?.let {
                    resumeLocalDataSource.insertUser(it)
                }
            }else if (response is Result.Error){
                Timber.d("Error")

                throw response.exception
            }
    }


    override suspend fun getRepos(owner: String): Result<List<Repo>?> {
        try {
            updateReposFromRemoteDataSource(owner)
        }catch (ex: Exception){
          ///  return Result.Error(ex)
        }

        return resumeLocalDataSource.getRepos(owner)
    }

    private suspend fun updateReposFromRemoteDataSource(owner: String) {
        val response = resumeRemoteDataSource.getRepos(owner)

        if (response is Result.Success){

            val repos = mutableListOf<Repo>()
            response.data?.forEach {
                it.login = owner
                repos.add(it)
            }

            resumeLocalDataSource.insertRepos(repos)
        }else if (response is Result.Error){
            throw response.exception
        }
    }

}