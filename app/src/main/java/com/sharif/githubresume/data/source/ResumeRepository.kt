package com.sharif.githubresume.data.source

import com.sharif.githubresume.vo.Repo
import com.sharif.githubresume.vo.Result
import com.sharif.githubresume.vo.User

interface ResumeRepository {

    suspend fun getUser(userName: String) : Result<User?>

    suspend fun getRepos(owner: String) : Result<List<Repo>?>

}