package com.sharif.githubresume.data.remote

import com.sharif.githubresume.api.GithubService
import com.sharif.githubresume.data.source.ResumeDataSource
import com.sharif.githubresume.vo.Repo
import com.sharif.githubresume.vo.Result
import com.sharif.githubresume.vo.User

class ResumeRemoteDataSource(private val githubService: GithubService): ResumeDataSource {

    override suspend fun getUser(userName: String): Result<User?> {
        return try {
           val response = githubService.getGithubUser(userName)
            if (response.isSuccessful) {
                Result.Success(response.body())
            } else {
                Result.Error(Exception(response.message()))
            }
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun getRepos(owner: String): Result<List<Repo>?> {
        return try {
            val response = githubService.getRepos(owner)
            if (response.isSuccessful){
                Result.Success(response.body())
            }else{
                Result.Error(Exception(response.message()))
            }
        }catch (e: Exception){
            Result.Error(e)
        }

    }

    override suspend fun insertUser(user: User) {}

    override suspend fun insertRepos(repos: List<Repo>) {}

}