package com.sharif.githubresume.data.local

import com.sharif.githubresume.db.AppDatabase
import com.sharif.githubresume.data.source.ResumeDataSource
import com.sharif.githubresume.vo.Repo
import com.sharif.githubresume.vo.Result
import com.sharif.githubresume.vo.User
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ResumeLocalDataSource(private val appDatabase: AppDatabase,
                            private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO): ResumeDataSource {

    override suspend fun getUser(userName: String): Result<User?> = withContext(ioDispatcher){
        return@withContext Result.Success(appDatabase.userDao().getUser(userName))
    }

    override suspend fun getRepos(owner: String): Result<List<Repo>?> = withContext(ioDispatcher){
        return@withContext Result.Success(appDatabase.repoDao().getRepos(owner))
    }

    override suspend fun insertUser(user: User) = withContext(ioDispatcher){
        appDatabase.userDao().insertUser(user)
    }

    override suspend fun insertRepos(repos: List<Repo>) = withContext(ioDispatcher){
        appDatabase.repoDao().insertRepos(repos)
    }

}