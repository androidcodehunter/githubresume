package com.sharif.githubresume.utils

import com.sharif.githubresume.vo.Repo

fun List<Repo>.sumKeyPercentage(): MutableMap<String, Int> {
    val map = mutableMapOf<String, Int>()
    val result = mutableMapOf<String, Int>()

    for (repo in this) {
        if (map.contains(repo.language)) {
            val key = repo.language
            key?.let {
                val value = map[it] ?: 1
                map.put(it, value + 1)
            }
        } else {
            repo.language?.let {
                if (it.isNotEmpty()) {
                    map[it] = 1
                }
            }
        }
    }


    val totalSum = map.map { it.value }.sum()
    for ((key, value) in map) {
        result[key] = ((value.toFloat() / totalSum.toFloat()) * 100).toInt()
    }


    return result
}