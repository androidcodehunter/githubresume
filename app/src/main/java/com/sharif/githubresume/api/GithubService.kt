package com.sharif.githubresume.api

import com.sharif.githubresume.vo.Repo
import com.sharif.githubresume.vo.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubService {

    @GET("users/{username}")
    suspend fun getGithubUser(@Path("username")username: String): Response<User>

    @GET("users/{login}/repos")
    suspend fun getRepos(@Path("login") login: String) : Response<List<Repo>>

    companion object{
        const val BASE_URL = "https://api.github.com/"
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
    }

}