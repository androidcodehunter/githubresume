package com.sharif.githubresume.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sharif.githubresume.vo.User

@Dao
interface UserDao {

    @Query("select * from user where login = :user")
    suspend fun getUser(user: String): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: User)

}