package com.sharif.githubresume.db

import androidx.room.*
import com.sharif.githubresume.vo.Repo

@Dao
interface RepoDao {
    @Query("select * from repo where login = :user")
    suspend fun getRepos(user: String): List<Repo>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRepos(repos: List<Repo>)
}