package com.sharif.githubresume

import android.app.Application
import com.sharif.githubresume.di.netWorkModule
import com.sharif.githubresume.di.resumeRepositoryModule
import com.sharif.githubresume.di.resumeViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class GitHubResumeApp : Application(){
    override fun onCreate() {
        super.onCreate()
        startKoinForDependency()
        //Initiate Timber for better logging
        Timber.plant(Timber.DebugTree())
    }

    /**
     * Initialize all dependencies here.
     * All dependency related code available in @di package.
     */
    private fun startKoinForDependency() {
        startKoin {
            androidContext(this@GitHubResumeApp)
            modules(listOf(netWorkModule,
                resumeRepositoryModule,
                resumeViewModel))
        }
    }


}