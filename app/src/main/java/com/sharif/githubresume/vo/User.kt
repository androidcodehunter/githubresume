package com.sharif.githubresume.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.lang.StringBuilder
import java.util.*

@Entity
data class User(
    @PrimaryKey
    val login: String,
    val avatar_url: String?,
    val name: String?,
    val blog: String?,
    val location: String?,
    val public_repos: String?,
    val followers: String?,
    val created_at: Date?
) {

    fun createProfileDescription(): String {
        val profile = StringBuilder()
        profile.append("On GitHub since ")
        profile.append(created_at)
        profile.append(", ")
        profile.append(name)
        profile.append(" is a developer")

        location?.let {
            profile.append(" based in ")
            profile.append(it)
        }

        var repoAvailable = true
        public_repos?.let {
            repoAvailable = false
            profile.append(" with ")
            if (it.toInt() > 1) {
                profile.append("$it repositories")
            } else {
                profile.append("$it repository")
            }
        }

        followers?.let {
            if (repoAvailable) {
                profile.append(" with ")
            } else {
                profile.append(" and ")
            }

            if (it.toInt() > 1) {
                profile.append("$it followers")
            } else {
                profile.append("$it follower")
            }

        }
        return profile.toString()
    }
}