package com.sharif.githubresume.vo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Repo(
    @PrimaryKey
    val id: String,
    var login: String,
    val name: String,
    val description: String?,
    val language: String?,
    val html_url: String?)