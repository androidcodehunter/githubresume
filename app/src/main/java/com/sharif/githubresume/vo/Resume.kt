package com.sharif.githubresume.vo

data class Resume(var name: String? = "",
                  var description: String? = "",
                  var avatarUrl: String? = "",
                  var website: String? = "",
                  var repos: List<Repo>? = mutableListOf(),
                  var language: Map<String, Int>? = mutableMapOf())