package com.sharif.githubresume.ui

import androidx.lifecycle.*
import com.sharif.githubresume.data.source.ResumeRepository
import com.sharif.githubresume.utils.sumKeyPercentage
import com.sharif.githubresume.vo.Result
import com.sharif.githubresume.vo.Resume
import timber.log.Timber

class ResumeViewModel(private val resumeRepository: ResumeRepository) : ViewModel() {

    private var resumes: LiveData<Result<Resume>>

    private var githubUsername = MutableLiveData<String>()


    init {

        resumes = Transformations.switchMap<String, Result<Resume>>(githubUsername) { username ->
            liveData {

                emit(Result.Loading)

                try {
                    val userResponse = resumeRepository.getUser(username)
                    val repoResponse = resumeRepository.getRepos(username)

                    if (userResponse is Result.Success && repoResponse is Result.Success) {

                        var resume = Resume()
                        userResponse.data?.apply {
                            resume.name = name
                            resume.description = createProfileDescription()

                            blog?.let{
                                if (!it.startsWith("http://") && !it.startsWith("https://") && it.isNotEmpty()){
                                    resume.website = "http://$it"
                                }else{
                                    resume.website = it
                                }
                            }
                            resume.avatarUrl = avatar_url
                        }

                        repoResponse.data?.let { repos ->
                            resume.repos = repos
                            resume.language = repos.sumKeyPercentage()
                        }

                        emit(Result.Success(resume))
                    } else {
                        Timber.d("Something went wrong")
                        emit(Result.Error(Exception("Something went wrong")))
                    }
                } catch (ex: Exception) {
                    Timber.d("Exception $ex" )
                    emit(Result.Error(Exception(ex)))
                }
            }
        }
    }

    fun generateResume(userName: String) {
        githubUsername.value = userName
    }

    fun getResume(): LiveData<Result<Resume>> {
        return resumes
    }

    @Suppress("UNCHECKED_CAST")
    class ResumeViewModelFactory(private val resumeRepository: ResumeRepository) :
        ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            (ResumeViewModel(resumeRepository) as T)
    }

}