package com.sharif.githubresume.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import coil.api.load
import coil.transform.CircleCropTransformation
import com.google.android.material.chip.Chip
import com.sharif.githubresume.R
import com.sharif.githubresume.ui.MainActivity.Companion.GITHUB_USER_NAME
import com.sharif.githubresume.ui.adapter.RepoAdapter
import com.sharif.githubresume.vo.Repo
import com.sharif.githubresume.vo.Result
import com.sharif.githubresume.vo.Resume
import kotlinx.android.synthetic.main.fragment_resume.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ResumeFragment: Fragment() {

    private val resumeViewModel: ResumeViewModel by viewModel()
    private lateinit var repoAdapter: RepoAdapter

    private val resumeObserver = androidx.lifecycle.Observer<Result<Resume>>{ result ->
        if (result is Result.Loading){
            showLoading()
        }else if (result is Result.Success){
            hideLoading()
            showResume(result.data)
        }else{
            hideLoading()
            showError()
        }
    }

    private fun showResume(resume: Resume) {

        tvName.text = resume.name

        if (resume.name.isNullOrEmpty()){
            showError()
        }

        tvDescription.text = resume.description
        avatar.load(resume.avatarUrl){
            crossfade(true)
            placeholder(R.mipmap.ic_launcher_round)
            transformations(CircleCropTransformation())
        }

        if (resume.website.isNullOrEmpty()){
            tvWebsite.visibility = GONE
        }else{
            tvWebsite.visibility = VISIBLE
            tvWebsite.text = resume.website
        }

        showRepositories(resume.repos)
        showLanguage(resume.language)
    }

    private fun showLanguage(language: Map<String, Int>?) {
        language?.let {
            for ((key, value) in it){
                val chip = inflate(context, R.layout.tag, null) as Chip
                chip.text = " $key $value %"
                chips.addView(chip)
            }
        }

        if (language.isNullOrEmpty()){
            tvLanguageLabel.visibility = GONE
        }else{
            tvLanguageLabel.visibility = VISIBLE
        }
    }

    private fun showRepositories(repos: List<Repo>?) {
        if (repos.isNullOrEmpty()){
            tvRepositoryLabel.visibility = GONE
        }else{
            tvRepositoryLabel.visibility = VISIBLE
        }
        repoAdapter.submitList(repos)
    }

    private fun showError() {
        Toast.makeText(context, getString(R.string.not_found), Toast.LENGTH_SHORT).show()
        activity?.finish()
    }

    private fun hideLoading() {
        progressbar.visibility = GONE
        cardContainer.visibility = VISIBLE
    }

    private fun showLoading() {
        progressbar.visibility = VISIBLE
        cardContainer.visibility = GONE
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_resume, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvWebsite.setOnClickListener {
            openUrlInBrowser(tvWebsite.text.toString())
        }
        repoAdapter = RepoAdapter {
            url -> openUrlInBrowser(url)
        }
        repositories.apply {
            setHasFixedSize(true)
            adapter = repoAdapter
        }
    }

    private fun openUrlInBrowser(url: String) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        resumeViewModel.getResume().observe(viewLifecycleOwner, resumeObserver)
        val githubUserName = arguments?.getString(GITHUB_USER_NAME)
        githubUserName?.run {
            resumeViewModel.generateResume(this)
        }
    }

    companion object{
        fun newInstance(gitHubUsername: String): Fragment{
            val fragment = ResumeFragment()
            val bundle = Bundle()
            bundle.putString(GITHUB_USER_NAME, gitHubUsername)
            fragment.arguments = bundle
            return fragment
        }
    }

}