package com.sharif.githubresume.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sharif.githubresume.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnGenerate.setOnClickListener {
            if (etGitHubUserName.text.toString().isEmpty()) {
                etGitHubUserName.error = getString(R.string.error_msg_github_username_empty)
            } else {
                startGenerateResumeActivity(etGitHubUserName.text.toString())
            }
        }
    }

    private fun startGenerateResumeActivity(userName: String) {
        val intent = Intent(this, ResumeActivity::class.java)
        intent.putExtra(GITHUB_USER_NAME, userName)
        startActivity(intent)
    }

    companion object {
        const val GITHUB_USER_NAME = "github_user_name"
    }
}
