package com.sharif.githubresume.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sharif.githubresume.R
import com.sharif.githubresume.ui.MainActivity.Companion.GITHUB_USER_NAME

class ResumeActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resume)

        intent.getStringExtra(GITHUB_USER_NAME)?.apply {
            supportFragmentManager.beginTransaction()
                .add(R.id.resumeContainer, ResumeFragment.newInstance(this))
                .commit()
        }



    }

}