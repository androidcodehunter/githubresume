package com.sharif.githubresume.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sharif.githubresume.R
import com.sharif.githubresume.vo.Repo
import kotlinx.android.synthetic.main.list_item_repository.view.*

class RepoAdapter(val onRepoClickListener: (url: String) -> Unit) : ListAdapter<Repo, RepoAdapter.ViewHolder>(REPO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_repository, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                getItem(adapterPosition).html_url?.let{
                    onRepoClickListener(it)
                }
            }
        }

        fun bindTo(repo: Repo) {
            itemView.tvRepoName.text = repo.name
            if (!repo.description.isNullOrEmpty()) {
                itemView.tvRepoDescription.visibility = VISIBLE
                itemView.tvRepoDescription.text = repo.description
            } else {
                itemView.tvRepoDescription.visibility = GONE
            }
        }
    }


    companion object {
        /**
         * Repo comparator to check for new data updates only, it will ignore duplicate data update.
         * This technique is very effective when you update data continuously.
         */
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Repo>() {
            override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean {
                return oldItem == newItem
            }
        }
    }


}