package com.sharif.githubresume.api

import com.sharif.githubresume.utils.sumKeyPercentage
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class GithubServiceTest {

    private lateinit var service: GithubService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService(){
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GithubService::class.java)
    }

    @After
    fun stopService(){
        mockWebServer.shutdown()
    }

    @Test
    fun getUserAndParseProfileTest() = runBlocking{
        //val userResponse = service.getGithubUser("defunkt")
        val userResponse = service.getGithubUser("defunkt")
        // mxcl, androidcodehunter
        if (userResponse.isSuccessful){
            val user = userResponse.body()
            println(user)
            println(user?.createProfileDescription())
        }
    }

    @Test
    fun getRepos() = runBlocking{
        service.getRepos("androidcodehunter")
    }



    @Test
    fun checkLanguagePercentageForGithubUser()= runBlocking{
        val repoResponse = service.getRepos("defunkt")

        if (repoResponse.isSuccessful){



                for ((key, value) in repoResponse.body()?.sumKeyPercentage()!!) {
                    println("$key $value")

                }

                //end of success
            }
        }





}