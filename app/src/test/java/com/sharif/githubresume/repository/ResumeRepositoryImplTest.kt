package com.sharif.githubresume.repository

import android.content.Context
import com.sharif.githubresume.data.source.ResumeRepositoryImpl
import com.sharif.githubresume.di.netWorkModule
import com.sharif.githubresume.di.resumeRepositoryModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.inject
import org.mockito.Mockito.mock


@RunWith(JUnit4::class)
class ResumeRepositoryImplTest: AutoCloseKoinTest(){

    val resumeRepository: ResumeRepositoryImpl by inject()

    @Before
    fun before(){
        startKoin {
            androidContext(mock(Context::class.java))
            modules(listOf(netWorkModule,
                resumeRepositoryModule))
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getRepo() = runBlocking {
        val response = resumeRepository.getRepos("androidcodehunter")
        println(response)
    }
}