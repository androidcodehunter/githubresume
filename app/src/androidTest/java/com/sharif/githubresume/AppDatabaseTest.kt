package com.sharif.githubresume

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sharif.githubresume.db.AppDatabase
import com.sharif.githubresume.db.UserDao
import com.sharif.githubresume.vo.User
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest{

    private lateinit var userDao: UserDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb(){
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        userDao = db.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }


    @Test
    @Throws
    fun insertUserAndCheckUser() = runBlocking{
        //login=androidcodehunter, avatar_url=https://avatars1.githubusercontent.com/u/4136921?v=4, name=Sharifur Rahaman, blog=, location=Dhaka, public_repos=50, followers=6, created_at=Fri Apr 12 12:52:15 GMT+06:00 2013
        val user = User("androidcodehunter",
            "https://avatars1.githubusercontent.com/u/4136921?v=4",
            "Sharifur Rahaman",
            "",
            "Dhaka",
            "50",
            "6",
            Date())

        userDao.insertUser(user)

        val newUser = userDao.getUser("androidcodehunter")
        assertThat(newUser, equalTo(user))
    }

}